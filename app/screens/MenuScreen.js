import React, {Component} from 'react'
import {
    Text,
    View,
    StyleSheet
} from 'react-native'

import SideMenu from 'react-native-side-menu'

import Header from '../components/Header'
import Menu from '../components/Menu'

class MenuScreen extends Component {

  constructor(props){
    super(props)
    this.state = {
        isOpen: false
    }
  }
  toggle(){
      this.setState({
          isOpen: !this.state.isOpen
      })
  }
  updateMenu(isOpen){
      this.setState({isOpen})
  }


  render(){
    return(
      <View style={[{flex:1},styles.container]}>
        <SideMenu
          menu={<Menu/>}
          isOpen={this.state.isOpen}
          onChange={(isOpen) => this.updateMenu(isOpen)}
        >
        <View style={[{flex: 1}, styles.container]}>
          <Header toggle={this.toggle.bind(this)}/>
        </View>
      </SideMenu>
      
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor: 'black'
  }
})

export default MenuScreen